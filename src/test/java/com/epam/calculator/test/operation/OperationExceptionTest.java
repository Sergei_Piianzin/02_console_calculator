package com.epam.calculator.test.operation;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.epam.calculator.operation.OperationException;

public class OperationExceptionTest {

  @Test
  public void shouldAssignMessageInConstructor() {
    String message = new OperationException("Something wrong").getMessage();
    assertThat(message, is("Something wrong"));
  }
}
