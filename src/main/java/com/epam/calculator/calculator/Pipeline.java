package com.epam.calculator.calculator;

import com.epam.calculator.operation.OperationException;

public class Pipeline {
  final static Character[] OPERATIONS_IN_ORDER = {'*', '/', '+'};

  public double count(String source) throws OperationException, NumberFormatException, CalculationException {
    Calculator calculator = new Calculator();
    for (Character operation : OPERATIONS_IN_ORDER) {
      int operatorCount =
          new Long(source.chars().mapToObj(i -> (char) i).filter(c -> c.equals(operation)).count())
              .intValue();
      for (int i = 0; i < operatorCount; ++i) {
        source = calculator.calculate(operation, source);
      }
    }
    return Double.parseDouble(source);
  }
}
