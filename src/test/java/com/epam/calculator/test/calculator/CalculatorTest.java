package com.epam.calculator.test.calculator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
  CalculatorPositiveTest.class,
  CalculatorNegativeTest.class
})
public class CalculatorTest {
}
