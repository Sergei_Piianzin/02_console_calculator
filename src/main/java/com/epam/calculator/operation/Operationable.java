package com.epam.calculator.operation;

public interface Operationable<T> {
  T count(T t1, T t2);
}
