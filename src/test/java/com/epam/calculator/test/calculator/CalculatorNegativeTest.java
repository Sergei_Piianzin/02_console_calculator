package com.epam.calculator.test.calculator;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.calculator.calculator.CalculationException;
import com.epam.calculator.calculator.Calculator;
import com.epam.calculator.operation.OperationException;

public class CalculatorNegativeTest {

  private static Calculator calculator;

  @BeforeClass
  public static void init() {
    calculator = new Calculator();
  }

  @AfterClass
  public static void tearDown() {
    calculator = null;
  }

  @Test(expected = IllegalArgumentException.class)
  public void divisionByZero()
      throws NumberFormatException, OperationException, CalculationException {
    String expression = "42/0";
    calculator.calculate('/', expression);
  }

  @Test(expected = CalculationException.class)
  public void emptyExpressionThrows()
      throws NumberFormatException, OperationException, CalculationException {
    String expression = "";
    calculator.calculate('+', expression);
  }

  @Test(expected = OperationException.class)
  public void badOperationThrows()
      throws NumberFormatException, OperationException, CalculationException {
    String expression = "42+42";
    calculator.calculate(':', expression);
  }

  @Test(expected = CalculationException.class)
  public void multiOperationThrows()
      throws NumberFormatException, OperationException, CalculationException {
    String expression = "42++42";
    calculator.calculate('+', expression);
  }
}
