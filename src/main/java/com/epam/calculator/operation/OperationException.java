package com.epam.calculator.operation;

public class OperationException extends Exception {
  private String message;

  public OperationException(String message) {
    this.message = message;
  }

  public OperationException(Character c) {
    message = c.toString() + " is not allowable operation!";
  }

  public String getMessage() {
    return message;
  }
}
