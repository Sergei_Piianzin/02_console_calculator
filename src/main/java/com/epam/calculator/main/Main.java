package com.epam.calculator.main;

import java.util.Scanner;

import com.epam.calculator.calculator.CalculationException;
import com.epam.calculator.calculator.Pipeline;
import com.epam.calculator.operation.OperationException;

public class Main {
  public static void main(String[] args) {
    Main loop = new Main();
    while (true) {
      try {
        loop.run();
        break;
      } catch (OperationException e) {
        System.out.println(e.getMessage());
      } catch (NumberFormatException e) {
        System.out.println("Bad input");
      } catch (CalculationException e) {
        System.out.println(e.getReason());
        e.printStackTrace();
      }
    }
  }

  public void run() throws OperationException, NumberFormatException, CalculationException {
    Scanner input = new Scanner(System.in);
    System.out.println("Input the term:");
    mainLoop(input);
    input.close();
    System.out.println("Good day");
  }

  private void mainLoop(Scanner input) throws OperationException, CalculationException {
    while (input.hasNextLine()) {
      String inputLine = input.nextLine();
      if (inputLine.equals("q")) {
        break;
      }
      double result = new Pipeline().count(inputLine);
      System.out.println("Result: " + result);
      System.out.println("Input the term (q - for exit):");
    }
  }
}


