package com.epam.calculator.test.calculator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.epam.calculator.calculator.CalculationException;
import com.epam.calculator.calculator.Pipeline;
import com.epam.calculator.operation.OperationException;

public class PipelineTest {
  
  @Test
  public void shouldCalculateExpression() throws NumberFormatException, OperationException, CalculationException {
    double result = new Pipeline().count("10+-10+5*4/2");
    assertThat(result, is(10.0d));
  }
}
