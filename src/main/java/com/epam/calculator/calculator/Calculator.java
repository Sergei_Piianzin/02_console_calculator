package com.epam.calculator.calculator;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.calculator.operation.Operation;
import com.epam.calculator.operation.OperationException;
import com.epam.calculator.operation.Operationable;

public class Calculator {
  public String calculate(Character operator, String source)
      throws OperationException, NumberFormatException, CalculationException {
    Operationable<Double> op = Operation.getOperation(operator);

    ArrayList<String> listOfTerms = getTermList(operator, source);

    String leftTerm = listOfTerms.get(0);
    String leftOperand = getLastNumber(leftTerm);
    double leftVal = Double.parseDouble(leftOperand);

    String rightTerm = listOfTerms.get(1);
    String rightOperand = getFirstNumber(rightTerm);
    double rightVal = Double.parseDouble(rightOperand);

    Double result = op.count(leftVal, rightVal);

    String begin = leftTerm.substring(0, leftTerm.length() - leftOperand.length());
    String end = rightTerm.substring(rightOperand.length(), rightTerm.length());

    StringBuilder resultString = new StringBuilder(begin + result + end);
    for (int i = 2; i < listOfTerms.size(); ++i) {
      resultString.append(operator).append(listOfTerms.get(i));
    }

    return resultString.toString();
  }

  private ArrayList<String> getTermList(Character operator, String source)
      throws OperationException, CalculationException {
    String delimiter = getDelimiter(operator);
    Scanner terms = new Scanner(source);
    terms.useDelimiter(delimiter);
    ArrayList<String> listOfTerms = new ArrayList<>();
    while (terms.hasNext()) {
      String term = terms.next();
      if (term.isEmpty()) {
        throw new CalculationException(source);
      }
      listOfTerms.add(term);
    }

    if (listOfTerms.size() < 2) {
      throw new CalculationException(source);
    }

    terms.close();
    return listOfTerms;
  }

  private String getDelimiter(Character operator) {
    String left = "\\s*";
    String right = "\\s*";
    if (operator.equals('-')) {
      return left + operator.toString() + right;
    } else {
      return left + "\\" + operator.toString() + right;
    }

  }

  private String getLastNumber(String term) {
    Matcher m = getValueMatcher(term);
    String result = "";
    while (m.find()) {
      result = m.group();
    }
    return result;
  }

  private Matcher getValueMatcher(String term) {
    Pattern p = Pattern.compile("-?[\\d\\.]+");
    Matcher m = p.matcher(term);
    return m;
  }

  private String getFirstNumber(String term) {
    Matcher m = getValueMatcher(term);
    String result = "";
    if (m.find()) {
      result = m.group();
    }
    return result;
  }
}
