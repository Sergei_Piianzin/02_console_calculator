package com.epam.calculator.operation;

public class Operation {
  public static Operationable<Double> getOperation(Character operation) throws OperationException {
    switch (operation) {
      case '+':
        return (x, y) -> x + y;
      case '-':
        return (x, y) -> x - y;
      case '*':
        return (x, y) -> x * y;
      case '/':
        return (x, y) ->  { 
          if (y == 0) throw new IllegalArgumentException();
          return x / y;
        };
      default:
        throw new OperationException(operation);
    }
  }
}
