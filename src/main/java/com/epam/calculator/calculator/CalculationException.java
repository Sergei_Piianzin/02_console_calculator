package com.epam.calculator.calculator;

public class CalculationException extends Exception{
  private static final long serialVersionUID = 1L;
  private String reason;
  
  public CalculationException() {
  }
  
  public CalculationException(String reason) {
    this.reason = reason;
  }
  
  public String getReason() {
    return reason;
  }
  
}
