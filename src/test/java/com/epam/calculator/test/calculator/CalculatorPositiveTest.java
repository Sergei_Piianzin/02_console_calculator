package com.epam.calculator.test.calculator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.calculator.calculator.CalculationException;
import com.epam.calculator.calculator.Calculator;
import com.epam.calculator.operation.OperationException;

public class CalculatorPositiveTest {
  private final static Character PLUS = '+';
  private final static Character MINUS = '-';
  private final static Character MULT = '*';
  private final static Character DIV = '/';

  private static Calculator calculator;

  @BeforeClass
  public static void init() {
    calculator = new Calculator();
  }

  @AfterClass
  public static void tearDown() {
    calculator = null;
  }

  @Test
  public void shouldReturnSum()
      throws NumberFormatException, OperationException, CalculationException {
    String expression = "2+2";
    double result = Double.parseDouble(calculator.calculate(PLUS, expression));
    assertThat(result, is(2.0d + 2.0d));
  }

  @Test
  public void shouldReturnResidual()
      throws NumberFormatException, OperationException, CalculationException {
    String expression = "10-5";
    double result = Double.parseDouble(calculator.calculate(MINUS, expression));
    assertThat(result, is(10.0d - 5.0d));
  }

  @Test
  public void shouldReturnMult()
      throws NumberFormatException, OperationException, CalculationException {
    String expression = "10*0.25";
    double result = Double.parseDouble(calculator.calculate(MULT, expression));
    assertThat(result, is(10 * 0.25));
  }

  @Test
  public void shouldReturnDiv()
      throws NumberFormatException, OperationException, CalculationException {
    String expression = "90/33";
    double result = Double.parseDouble(calculator.calculate(DIV, expression));
    assertThat(result, is(90.0d / 33.0d));
  }
}
